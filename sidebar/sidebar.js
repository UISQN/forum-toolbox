
let thisWindow = 0;

const addData = (s) => {
    let taData = document.getElementById("taData");
    taData.value = taData.value + s + "\n";
    taData.scrollTop = taData.scrollHeight;
};

const urlUpdated = (url) => {
    let match = url.match(/(https?:\/\/[a-zA-Z-.]+\/)f(\d+)-[a-z-]+\.html/);
    if (!match) {
        match = url.match(/(https?:\/\/[a-zA-Z-.]+\/)forumdisplay.php\?f=(\d+).*/)
    }

    if (match) {
        document.getElementById("inputBaseURL").value = match[1];
        document.getElementById("inputForumId").value = match[2];
    }
};

const updateTabData = () => {
    browser.tabs.query({active: true, windowId: thisWindow}).then((tabs => {
        urlUpdated(tabs[0].url);
    }));
};

const forumThreadList = () => {
    document.getElementById("btnStart").disabled = true;
    document.getElementById("tabHint").style.visibility = 'visible';
    let baseURL = document.getElementById("inputBaseURL").value;
    let forumId = +document.getElementById("inputForumId").value;
    let pageFrom = +document.getElementById("inputPageFrom").value;
    let pageTo = +document.getElementById("inputPageTo").value;
    browser.runtime.sendMessage({
        command: "forumThreadList",
        parameter: {
            baseURL: baseURL,
            forumId: forumId,
            pageFrom: pageFrom,
            pageTo: pageTo
        }
    });
};

const init = () => {
    browser.windows.getCurrent({populate: true}).then((windowInfo) => {
        thisWindow = windowInfo.id;
        updateTabData();
    });

    browser.tabs.onActivated.addListener((activeInfo)=> {
        if (activeInfo.windowId === thisWindow) {
            updateTabData();
        }
    });

    browser.tabs.onUpdated.addListener((updateInfo) => {
        updateTabData();
    },{properties: ["status"]});

    browser.runtime.onMessage.addListener((message) => {
        switch (message.command) {
            case 'data':
                break;
            case 'finished':
            case 'error':
                document.getElementById("btnStart").disabled = false;
                document.getElementById("tabHint").style.visibility = 'collapse';
                break;
        }
        if (message.parameter.data) {
            addData(message.parameter.data);
        }
    });

    document.getElementById("btnStart").addEventListener("click", e => {
        forumThreadList();
    });
    document.getElementById("btnStop").addEventListener("click", e => {
        let taData = document.getElementById("taData");
        taData.value = taData.value + "Stopping...waiting...\n";
        browser.runtime.sendMessage({
            command: "stop",
        });
    });
    document.getElementById("btnClear").addEventListener("click", e => {
        let taData = document.getElementById("taData");
        taData.value = "";
    });

};

init();
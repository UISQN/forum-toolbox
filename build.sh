#!/bin/sh

rm forum-toolbox*.zip

VERSION=$(cat version)
echo Building $VERSION

zip  forum-toolbox-${VERSION}.zip -r  background/ contentscripts/ icons/ manifest.json sidebar/

#!/usr/bin/env bash

VERSION=$(cat version)
echo Setting version $VERSION

awk -v version=$VERSION '/"version"/ {print "  \"version\": \"" version "\","} !/"version"/ {print}' manifest.json > manifest.json.new
mv manifest.json.new manifest.json

awk -v version=$VERSION '/VERSION/ {print "<!-- VERSION --> " version} !/VERSION/ {print}' sidebar/sidebar.html > sidebar/sidebar.html.new
mv sidebar/sidebar.html.new sidebar/sidebar.html


stop = false;
currentTimeout = null;

const sendData = (s, error=false) => {
    browser.runtime.sendMessage({
        command: error? "error" : "data",
        parameter: {
            data: s
        }
    });
};

const imageMD5 = async (url) => {
    console.log(url);
    fetch(url, {method: 'GET', credentials: 'include'})
        .then(response => {
            console.log("response");
            return response.blob();
        })
        .then(blob => {
            return blob.arrayBuffer();
        })
        .then(arrayBuffer => {
            let result = md5(arrayBuffer);
            sendData("MD5:" + result);
        })
        .catch(e => {
            e => sendData("Error: " + e.message, true);
        });
};

const parseForumPage = (forumId, pageNo, text) => {
    let doc = new DOMParser().parseFromString(text, "text/html");
    let smallFont = document.querySelector("div.smallfont");
    if (smallFont && smallFont.innerText.includes("not logged in")) {
        throw new Error("Error: Are you logged in?");
    }
    let table = doc.querySelector("table#threadslist");
    if (!table) {
        throw new Error("Parse-Error: thread table not found!\n" + text);
    }
    table.querySelectorAll("tr").forEach(tr => {
        let tds = tr.querySelectorAll("td");
        if (tds.length !== 6 && tds.length !== 7) {
            return;
        }
        if (tds[2].querySelector('img[src="images/misc/sticky.gif"]')) {
            return;
        }
        let linkToThread = tds[2].querySelector('a[id^="thread_title_"]');
        if (!linkToThread) {
            return;
        }
        let threadId = linkToThread["id"];
        threadId = threadId.substring(threadId.lastIndexOf('_') + 1);
        let threadTitle = linkToThread.innerText;
        sendData(forumId + "," + pageNo + "," + threadId + "," + threadTitle);
    });
}

const forumThreadList = async (baseURL, forumId, pageNo, pageTo) => {
    currentTimeout = null;
    if (stop) {
        browser.runtime.sendMessage({
            command: "finished",
            parameter: {
                data: "stopped!"
            }
        });
        return;
    }
    try {
        let w = await browser.windows.getCurrent();
        let tabs = await browser.tabs.query({active: true, windowId: w.id});
        await browser.tabs.executeScript(tabs[0].id, {file: "/contentscripts/threadlist.js"});
        await browser.tabs.executeScript(tabs[0].id, {code: 'downloadForumPage("' + baseURL + '", ' + forumId + ',' + pageNo + ');'});
        if (pageNo === pageTo) {
            setTimeout(() => browser.runtime.sendMessage({
                command: "finished",
                parameter: {
                    data: "finished!"
                }
            }), 3000);
        } else if (pageNo < pageTo && !stop) {
            currentTimeout = setTimeout(() => forumThreadList(baseURL, forumId, pageNo + 1, pageTo), 3000);
        }
    } catch (e) {
        sendData("Error: " + e.message, true);
    }
};

const init = () => {
    browser.contextMenus.create({
        id: "imageMD5",
        title: "Image MD5",
        contexts: ["image"]
    });

    browser.contextMenus.onClicked.addListener(function(info, tab) {
        switch (info.menuItemId) {
            case "imageMD5":
                if (info.mediaType === "image" && info.srcUrl) {
                    imageMD5(info.srcUrl);
                }
                break;
        }
    })

    browser.runtime.onMessage.addListener((message) => {
        switch (message.command) {
            case 'md5':
                imageMD5(message.parameter.url);
                break;
            case 'forumThreadList':
                stop = false;
                forumThreadList(message.parameter.baseURL, message.parameter.forumId, message.parameter.pageFrom, message.parameter.pageTo);
                break;
            case 'forum_page':
                parseForumPage(message.parameter.forumId, message.parameter.pageNo, message.parameter.pageContent);
                break;
            case 'forum-toolbox.test':
                sendData(message.parameter.test);
                break;
            case 'stop':
                if(currentTimeout) {
                    clearTimeout((currentTimeout));
                    currentTimeout = null;
                    browser.runtime.sendMessage({
                        command: "finished",
                        parameter: {
                            data: "stopped!"
                        }
                    });
                }
                stop = true;
                break;
        }
    });
};

init();
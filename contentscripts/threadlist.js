"use strict";

var downloadForumPage = (baseURL, forumId, pageNo) => {
        fetch(baseURL + "forumdisplay.php?f=" + forumId + " &order=desc&page=" + pageNo, {
            method: 'GET',
            credentials: 'include'
        })
            .then(response => {
                if (response.status !== 200) {
                    throw new Error("Response Status != 200: " + response.status + "\n" + response.statusText + "\n" + response.body);
                }
                return response.arrayBuffer();
            })
            .then(buffer => {
                let decoder = new TextDecoder("iso-8859-1");
                let text = decoder.decode(buffer);
                browser.runtime.sendMessage({
                    command: 'forum_page',
                    parameter: {
                        forumId: forumId,
                        pageNo: pageNo,
                        pageContent: text
                    }
                });
            })
           .catch(e => browser.runtime.sendMessage({
               command: 'error',
               parameter: {
                   data: e.message
               }
           }));
    };
